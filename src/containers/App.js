import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Page from '../components/Page'
import * as pageActions from '../actions/PageActions'

class App extends Component {
  render() {
    const { page } = this.props
    const { getUserWeather, getWeather, addCity, deleteCity, getCitiesFromLocal } = this.props.pageActions

    return <div className='row'>
      <Page city={page.city} getCities={getCitiesFromLocal} deleteCity = {deleteCity} addCity = {addCity} cities={page.cities} getUserWeather={getUserWeather} getWeather ={getWeather} fetching={page.fetching} error={page.error}/>
    </div>
  }
}

function mapStateToProps(state) {
  return {
    page: state.page
  }
}

function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
