import React, {PropTypes, Component} from 'react'
import NameForm from './AddForm';
import  List from './List';
import weather from '../api/weather_api'
export default class Page extends Component {
    componentDidMount() {
        this.props.getUserWeather();
        this.props.getCities();
    }

    render() {
        const {addCity, cities, city, fetching, error} = this.props
        return <div >
            <div>
                {
                    fetching ? <p> Загрузка...</p> : ''
                }
            </div>
            <div>
                { city ?<div> <p>{city.name}, {Math.round(city.main.temp) - 272}&deg;C, {city.weather[0].description}</p>
                            <img src={weather.url_image + city.weather[0].icon + '.png'}/>
                        </div>: '' }
            </div>
            <div>
                <NameForm addCity={addCity} cities={cities}/>
                <List deleteCity={this.props.deleteCity} cities={cities}/>
            </div>
            <div>{ error ? <div className='error'> Во время загрузки произошла ошибка</div> : '' }</div>
        </div>
    }
}

Page.propTypes = {
    getUserWeather: PropTypes.func.isRequired,
    getWeather: PropTypes.func.isRequired,
    getCities: PropTypes.func.isRequired
}
