import React from 'react';
import weather from '../api/weather_api';
class Items extends React.Component {

    render() {
        let cities = this.props.cities;
        let deleteCity = this.props.deleteCity;
        function createCities(entry, index) {
            return <li>
                <div key={index}>
                    <p>{entry.name}, {Math.round(entry.main.temp) - 272}&deg;C, {entry.weather[0].description}</p>
                    <div>
                        <img src={weather.url_image + entry.weather[0].icon + '.png'}/>
                        <input onClick={() => deleteCity(index, cities)} type='button' value='x'/>
                    </div>
                </div>
            </li>
        }

        let listItems = cities.map(createCities);

        return (
            <ul>
                {listItems}
            </ul>
        );
    }
}

export default Items